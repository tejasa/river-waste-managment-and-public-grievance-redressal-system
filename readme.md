This System is a desktop application targeted to improve the current state of pollution 
in rivers by analyzing the data of waste generated. Foremost aim of developing this
application is to ensure that the voice of public is actively heard and efficiently acted
upon as it gives the government operators a systematic platform to organise,
scrutinize and redress public complaints in fair and transparent way.

Languages used for developing this project are Java and MySQL.

Use river_mini.sql script for importing the database.

This project uses JxMaps Library